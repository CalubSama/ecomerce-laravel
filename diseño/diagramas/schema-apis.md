```mermaid
graph TD
A[ Apis ] -->|utilizadas por | B(La App de terceros)
B --> C{login}
C -->|Login/registro| D[Facebook]
C -->|Login/registro| E[Github]
C -->|Login/registro| F[fa:fa-car Twitter]
C -->|Login/registro| G[fa:fa-car Google]
B --> C2{Productos}
C2 -->|Api de productos| D2[bestbuy-wegmans-rapidapi]
C2 -->|Api de productos| D3[api-alegra]
B --> C5{Ventas}
C5 -->|Api de | D5[Api de pasarela de pago]
B --> C4{Comunicaciones}
C4 -->|Api de | D4[google-gmail]
C4 -->|Api de | E4[google-maps]
```