```mermaid
graph TD
A[ Apis ] -->|utilizadas por| T(La App de la App)
T -->|Api de | Da[Api-Mandadeo-Usuarios]
T -->|Api de | Ea[Api-Mandadeo-Productos]
T -->|Api de | Fa[Api-Mandadeo-Carrito]
T -->|Api de | Ga[Api-Mandadeo-Ventas]
T -->|Api de | Ha[Api-Mandadeo-Admin]
T -->|Api de | Ia[Api-Mandadeo-Comerciante]
```