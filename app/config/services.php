<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'facebook' =>[
        'client_id' => '2410319942596221',
        'client_secret' => 'da8c40fff22e1352a6e35d7e5fdc9cfc',
        'redirect' => 'http://127.0.0.1:8000/callback/facebook',
    ],
    'github' => [
        'client_id' => env('ff8d5dc1ee4738e123b0'),
        'client_secret' => env('737ef6b4fe7952fcb019de97d03513d94d4a3257'),
        'redirect' => 'http://localhost:8000/auth/github/callback',
    ],
    'twitter' => [
     'client_id' => 'i9EuI4HYSD9UEcabFHFwwdmTs',
     'client_secret' => 'bhDMUVGqvquG1BSsx3cY9HqfM3Ton0WUCLwLBkuwISIuh9AdPA',
     'redirect' => 'http://localhost:8000/callback/twitter',
    ],
    'google' => [
        'client_id' => '615719582608-ka50g2qkpoht7f2rii9244520mtq2mt4.apps.googleusercontent.com',
        'client_secret' => 'JjS3q0n7XRaE5u9xq8unpjiM',
        'redirect' => 'http://localhost:8000/auth/google/callback',
    ],
    'stripe' => [
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET')
    ],
];
