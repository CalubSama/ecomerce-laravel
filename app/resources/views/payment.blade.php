          @section('content')
          <div class="container">
            <div class="row">
              <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                  <div class="panel-heading">Payment</div>
                  <div class="panel-body">
                    <form action="" method="POST">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <script
                      src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                      data-key="pk_test_xxxxxxxxxxxxxx"
                      data-amount="1000"
                      data-name="Pago semanal"
                      data-description="10.00 $ "
                      data-image="https://stripe.com/img/documentation/checkout/marketplace.png">
                    </script>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endsection