@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">

            <div class="pull-right">
                <span ><strong>Create new product</strong></span>
                <a class="btn btn-success" href="{{ route('products.create')}}" title="Create a product"> <i class="fas fa-plus-circle"></i>
                    </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{$message}}</p>
        </div>
    @endif

    <table class="table table-bordered table-responsive-lg ">
        <tr class="bg-black text-white">
            <th>No</th>
            <th>Name</th>
            <th>image</th>
            <th>description</th>
            <th>Price</th>
            <th>barcode</th>
            <th>Actions</th>
        </tr>
        @foreach ($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->name }}</td>
                <td><img src="{{ asset('storage/images'.$product->image) }}" height="75" width="75" alt="" /></td>
                <td>{{ $product->description }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->barcode }}</td>
                <td>

                    <form action="{{ route('products.destroy', $product->id)}}" method="POST">

                         <a href="{{ route('products.show', $product->id)}}" title="show">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>
                        <a href="{{ route('products.edit', $product->id)}}">
                            <i class="fas fa-edit  fa-lg"></i>
                        </a>
                        @csrf
                        @method('DELETE')
                        <div name="eliminar">
                        <button name="btn-delete{{$product->id}}" type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>
                        </button>
                        </div>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $products->links() !!}

@endsection
