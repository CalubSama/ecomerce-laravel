@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Product</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('products.index')}}" title="Go back"> <i class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Error!</strong>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="" class="form-control" placeholder="New Name">
                </div>
            </div>
{{-- new data --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description</strong>
                    <textarea class="form-control" style="height:150px" name="description"
                        placeholder=" new description"></textarea>
                </div>
            </div>
{{-- new data --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>barcode</strong>
                    <textarea class="form-control" style="height:50px" name="barcode"
                        placeholder="New barcode"></textarea>
                </div>
            </div>
            {{-- new data --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Price</strong>
                    <input type="number" name="price" class="form-control" placeholder="New price"
                        value="">
                </div>
            </div>
            {{-- new data --}}
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Post Image:</strong>
                     <input type="file" name="product_image" class="form-control" placeholder="Post Title">
                    @error('image')
                      <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                   @enderror
                </div>
                <div class="form-group">
                  <img src="{{ Storage::url($product->product_image) }}" height="200" width="200" alt="" />
                </div>
            </div>
            {{-- send --}}
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button name="btn-enviar" type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection