<x-guest-layout >
    <x-jet-authentication-card>
        <x-slot name="logo">
            <img src="assets/images/logo-top-1.png" alt="logo" width="120px" height="  120px">
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label value="{{ __('Email') }}" />
                <x-jet-input class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label value="{{ __('Password') }}" />
                <x-jet-input class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label class="flex items-center">
                    <input type="checkbox" class="form-checkbox" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>
            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-jet-button class="ml-4">
                    {{ __('Login') }}
                </x-jet-button>
            </div>
            <div class="pl-15 items-center md:items-center self-center ">
                   <a href="{{ url('auth/github') }}">
                    <img src="https://help.dropsource.com/wp-content/uploads/sites/4/2017/02/gh-login-button.png" width="280px">
                </a>
                <a href="{{ url('auth/facebook') }}">
                    <img src="https://i.stack.imgur.com/oL5c2.png" width="280px" >
                </a>
                <a  href="{{ url('auth/twitter') }}">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ_5zI36Gqbr_ZqB0_0iFrBDzy8roS15EJb0A&usqp=CAU" width="280px" >
                </a>
                <a href="{{ url('auth/google') }}">
                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQY8xYeJ0g9vNLBNDUMbVif_9f_vcgZ9Z_fpQ&usqp=CAU" width="280px"  style="border-radius: 6px;">
                </a>
            </div>

            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>