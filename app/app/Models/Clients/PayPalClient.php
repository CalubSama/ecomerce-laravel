<?php
namespace App\Clients;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
class PayPalClient
{
    /**
     * Returns PayPal HTTP context instance with environment that has access
     * credentials context. Use this instance to invoke PayPal APIs, provided the
     * credentials have access.
     */
    public function context()
    {
        return new ApiContext($this->credentials());
    }
    /**
     * Set up and return PayPal PHP SDK environment with PayPal access credentials.
     *
     * Paste your client_id and client secret as below
     */
    protected function credentials()
    {
        $clientId     = 'Afr3RCxbyKS9kGFyBBBAPIRxbl2Vvdtew-kqJ10jkLYY_U_laN_sLcOoPe0ODQw8hld7Er6YI4YvUOw0';
        $clientSecret = 'EDBkZz5WXS6oQvz3i_zihmlEHdQrvrtb3u0fxPtDdr7EWk0rvDzC6VA2Zl_blHKn4eW-mCwYvu7nDcc2';
        return new OAuthTokenCredential($clientId, $clientSecret);
    }
}