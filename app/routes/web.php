<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\FacebookSocialiteController;
use App\Http\Controllers\GithubController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\StripePaymentController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Livewire\HomeComponent;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('products');
});

Route::get('products', [UserController::class, 'index']);
Route::resource('products', ProductController::class);


Route::get('public', HomeComponent::class);


Route::get('privacypolicies', function () {
    return view('privacypolicies');
});
Route::get('privacyterms', function () {
    return view('privacyterms');
});
Route::get('google53e086004b7620b7', function () {
    return view('google53e086004b7620b7.html');
});

//jeatstream routes
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
//api register facebook
Route::get('auth/facebook', [FacebookSocialiteController::class, 'redirectToFB']);
Route::get('callback/facebook', [FacebookSocialiteController::class, 'handleCallback']);
//api register Github
Route::get('auth/github', [GithubController::class, 'redirectToGithub']);
Route::get('auth/github/callback', [GithubController::class, 'handleGithubCallback']);
//api register Twitter
Route::get('auth/twitter', [LoginController::class, 'redirectToTwitter']);
Route::get('auth/twitter/callback', [LoginController::class,'handleTwitterCallback']);
//api register google
Route::get('auth/google', [GoogleController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);
//api register stripe
Route::get('stripe', [StripePaymentController::class, 'index']);
Route::post('payment-process', [StripePaymentController::class, 'process']);
//route home
Route::get('', function(){
	return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

